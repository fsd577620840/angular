import { Component, OnInit } from '@angular/core';

interface Product {
  name: string;
  description: string;
  price: number;
  image: string;
}

interface CartItem {
  product: Product;
  quantity: number;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css'
})
export class ProductsComponent {
  emailId: any;
  products: Product[] = [
    {
      name: 'DELL-Laptop',
      description: 'Inspiron 5502, Intel Core i5 Processor(11th Gen), 16 GB DDR4 RAM, 64 bit Windows 11 OS, 512 GB SSD, 15.6 Inch',
      price: 450.99,
      image: 'https://i.dell.com/is/image/DellContent/content/dam/ss2/product-images/dell-client-products/notebooks/inspiron-notebooks/16-5635-amd/media-gallery/non-touch/silver/in5635nt-cnb-00055lf110-sl-fpr.psd?fmt=pjpg&pscan=auto&scl=1&wid=3429&hei=2139&qlt=100,1&resMode=sharp2&size=3429,2139&chrss=full&imwidth=5000'
    },
    {
      name: 'INDIAN SCOUT BOBBER',
      description: 'Black Metallic, Liquid Cooled 1,133 cc V-twin, Closed loop fuel injection / 60 mm bore, 12.5 L, 124 km/h, 220-2,280 mm L x 926-995 mm W x 1,154-1,181 mm H ',
      price: 20331.40,
      image: 'https://www.indianmotorcycle.in/fileadmin/templates/model_24/swap/intl/scout-bobber/scout-bobber-black.jpg'
    },
   
    {
      name: 'VU TV',
      description: 'Vu Premium 139 cm(55 inch) Ultra HD(4K) LED Smart Android TV',
      price: 417.99,
      image: 'https://m.media-amazon.com/images/I/71aR9pGJkKL._AC_UF1000,1000_QL80_.jpg'
    },
    {
      name: 'PHILIPS SoundBar',
      description: 'TAB8967 7.1 Ch (5.1.2) Real Surround, Dolby Atmos, Wireless Subwoofer, UP-Firing Speakers, Wireless Rear Speakers, AI Voice Assistant, 780W (Black)',
      price: 481.99,
      image: 'https://images.philips.com/is/image/PhilipsConsumer/HTL2163B_12-IMS-en_IN?$jpglarge$&wid=1250'
    },
    {
      name: 'BOAT Airdopes',
      description: 'Atom 81 TWS Earbuds with Upto 50H Playtime, Quad Mics ENx™ Tech, 13MM Drivers,Super Low Latency(50ms), ASAP™ Charge, BT v5.3(Opal Black)',
      price: 29.99,
      image: 'https://m.media-amazon.com/images/I/61yyQD1KLOL.SY355.jpg'
    },

    {
      name: 'APPLE iPhone 15 Max Pro',
      description: 'MTP73HN/A, iOS 17, A16 Bionic Chip, 6 Core Processor, 256GB, 15.49 cm (6.1 inch)',
      price: 1259.00,
      image: 'https://assets.sangeethamobiles.com/product_img/14513/1694714990_HBr.jpg'
    },
    
  ];

  constructor() {
    this.emailId = localStorage.getItem('emailId');
  }
  ngOnInit(){

  }
}
