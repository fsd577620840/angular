import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent implements OnInit {

  empId: any;
  emp: any;
  employees: any;

  constructor() {
    this.employees = [
      {empId:1,empName:'Meghana',salary:50000.00,gender:'Female',doj:'2023-08-18',country:'INDIA',emailId:'meghana@gmail.com',password:'meghana@123'},
      {empId:2,empName:'Priyanka',salary:55000.00,gender:'Female',doj:'2021-08-11',country:'INDIA',emailId:'priyanka@gmail.com',password:'pichi@589'},
      {empId:3,empName:'Satya',salary:58000.00,gender:'Female',doj:'2018-05-25',country:'INDIA',emailId:'satya@gmail.com',password:'pilla@517'},
      {empId:4,empName:'Suman',salary:36000.00,gender:'Male',doj:'2020-07-01',country:'INDIA',emailId:'suman@gmail.com',password:'yedava@420'},
      {empId:5,empName:'Rohith',salary:35000.00,gender:'Male',doj:'2020-06-01',country:'INDIA',emailId:'rohith@gmail.com',password:'ghathana@521'},
    
    ];
  }

  ngOnInit() {
  }

  getEmployee(employee: any) {

    this.emp = null;

    this.employees.forEach((element: any) => {
      if (element.empId == employee.empId) {
        this.emp = element;
      }
    });
  }

}

