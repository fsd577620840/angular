import { Component, OnInit } from '@angular/core';
//Import Router class
import { Router } from '@angular/router';

//Import EmpService
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {

  emailId: any;
  password: any;
  employees: any;
  emp: any;

   //Dependency Injection for EmpService, Router
  constructor(private router: Router, private service: EmpService) {
    this.employees = [
      {empId:1, empName:'Meghana', salary:50000.00, gender:'Female', doj:'2023-08-18', country:'INDIA', emailId:'meghana@gmail.com', password:'@123'},
      {empId:2, empName:'Priyanka', salary:55000.00, gender:'Female', doj:'2021-08-11', country:'INDIA', emailId:'priyanka@gmail.com', password:'pichi@589'},
      {empId:3, empName:'Satya', salary:58000.00, gender:'Female', doj:'2018-05-25', country:'INDIA', emailId:'satya@gmail.com', password:'pilla@517'},
      {empId:4, empName:'Suman', salary:36000.00, gender:'Male', doj:'2020-07-01', country:'INDIA', emailId:'suman@gmail.com', password:'yedava@420'},
      {empId:5, empName:'Rohith', salary:35000.00, gender:'Male', doj:'2020-06-01', country:'INDIA', emailId:'rohith@gmail.com', password:'ghathana@521'}
    ]
  }

  ngOnInit(){
  }

  submit() {
    console.log("EmailId : " + this.emailId);
    console.log("Password: " + this.password);
  }

  loginSubmit(loginForm : any) {
    console.log(loginForm);
    console.log("EmailId : " + loginForm.emailId);
    console.log("Password: " + loginForm.password);

    if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {

      //Setting the isUserLoggedIn variable value to true under EmpService
      this.service.setIsUserLoggedIn();

      localStorage.setItem("emailId", loginForm.emailId);
      //Navigating/Routing to ShowEmployees Component
      this.router.navigate(['employee']);
    } else {

      this.emp = null;

      this.employees.forEach((element: any) => {
        if (element.emailId == loginForm.emailId && element.password == loginForm.password) {
          this.emp = element;
        }
      });

      if (this.emp != null) {

        //Setting the isUserLoggedIn variable value to true under EmpService
        this.service.setIsUserLoggedIn();

        localStorage.setItem("emailId", loginForm.emailId);
        //Navigating/Routing to Products Component
        this.router.navigate(['products']);
      } else {
        alert('Invalid Credentials');
      }
    }
  }
}