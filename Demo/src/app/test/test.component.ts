import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit{

  id: number;
  name: string;
  avg: number;
  address: any;
  hobbies: any;
  
  constructor() {

    //alert("constractor Invoked..");
    this.id = 101;
    this.name = 'Meghana';
    this.avg = 45.56;
    this.address = {
      streetNo: 101,
      city: 'Hyderabad',
      state: 'Telangana'

    };
    this.hobbies = ['Sleeping', 'Eating', 'Roaming'];
  }
  ngOnInit() {
    //  alert("ngOnInit Invoked..");
  }
}
